﻿using System.Web;
using System.Web.Optimization;

namespace AracKiralama
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/jquery-2.2.4.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/jquery.countTo.js",
                        "~/Scripts/idangerous.swiper.min.js",
                        "~/Scripts/equalHeightsPlugin.js",
                        "~/Scripts/jquery.datetimepicker.full.min.js",
                        "~/Scripts/bootstrap-select.min.js",
                        "~/Scripts/index.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/css_reset.css",
                        "~/Content/bootstrap.min.css",
                        "~/Content/jquery.datetimepicker.min.css",
                        "~/Content/bootstrap-select.min.css",
                        "~/Content/loaders.min.css",
                        "~/Content/index.css"
                      ));
        }
    }
}
